import { Component, OnInit } from '@angular/core';
import { Data } from '../../models/Data';
import { DataService } from '../../services/data.service';
import {AngularFireStorage} from '@angular/fire/storage';

@Component({
  selector: 'app-list-files',
  templateUrl: './list-files.component.html',
  styleUrls: ['./list-files.component.css']
})
export class ListFilesComponent implements OnInit {
  dataFiles: Data[];

  constructor(private dataService: DataService, private storage: AngularFireStorage) { }

  ngOnInit() {
    this.dataService.getDataFiles().subscribe(dataFiles => this.dataFiles = dataFiles);
  }

  download(path) {
    this.storage.ref(path).getDownloadURL().subscribe(value => window.location.href = value);
  }
}
