import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import  { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);
  hide = true;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }

  getEmailErrorMessage() {
    return this.email.hasError('required') ? 'You must enter a valid email address' :
      this.email.hasError('email') ? 'Not a valid email' : '';
  }
  getPassErrorMessage() {
    return this.password.hasError('required') ? 'You must enter a password' : '';
  }
  onSubmit() {
    this.authService.register(this.email.value, this.password.value).then(
      res => {
        this.router.navigate(['/dashboard']);
      });
  }
}
