export interface Data {
  id?: string;
  fileName: string;
  userEmail: string;
  time: number;
  path: string;
  size: number;
}
