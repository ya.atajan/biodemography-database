import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Data } from '../models/Data';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  dataFileCollection: AngularFirestoreCollection<Data>;
  dataFileDoc: AngularFirestoreDocument<Data>;
  dataFiles: Observable<Data[]>;
  data: Observable<Data>;

  constructor(private afs: AngularFirestore) {
    this.dataFileCollection = this.afs.collection('data', ref => ref.orderBy('fileName', 'asc'));
  }
  getDataFiles(): Observable<Data[]> {
    // Get clients by id
    this.dataFiles = this.dataFileCollection.snapshotChanges().pipe(
      map(changes => changes.map(action => {
        const data = action.payload.doc.data() as Data;
        data.id = action.payload.doc.id;
        return data;
      }))
    );

    return this.dataFiles;
  }
  getDataFile(id: string): Observable<Data> {
    this.dataFileDoc = this.afs.doc<Data>(`data/${id}`);
    this.data = this.dataFileDoc.snapshotChanges().pipe(
      map(action => {
        if (action.payload.exists === false) {
          console.log('PAYLOAD IS NULL');
          return null;
        } else {
          const data = action.payload.data() as Data;
          data.id = action.payload.id;
          return data;
        }
      }));

    return this.data;
  }
}
